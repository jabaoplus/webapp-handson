# typescript-webapp-handson
Typesctipt webapp hands on.

## Install
```
$ yarn
```

## Debug
Run the following commands in several terminals.
```
$ yarn build:dev:server
```
```
$ yarn start:dev:server
```

## Build Production And Run
```
$ yarn start
```

## Reference Project
https://github.com/IgnorantCoder/typescript-express-sample  
Thanks!

